module.exports = function (config, dependencies, job_callback) {
	var isFlow = function (date, data) {
		var time = date.getHours() * 100 + date.getMinutes();
		for (var i = 0; i < data.length; i++) {
			var flowPeriod = data[i];
			if (flowPeriod.start <= time && flowPeriod.end >= time) {
				return true;
			}
		}
		return false;
	}

	var prefixZero = function (val) {
		return (val < 10 ? '0' : '') + val;
	};

	var actDate = new Date();

	if (!config.flowOnly) {
		var dateStr = prefixZero(actDate.getDate())
			+ '-' + prefixZero(actDate.getMonth() + 1)
			+ '-' + actDate.getFullYear();	

		var dataObj = {
			isFlow: isFlow(actDate, config.flowHours),
			hour: prefixZero(actDate.getHours()),
			minutes: prefixZero(actDate.getMinutes()),
			dateStr: dateStr
		};
	} else {
		var dataObj = {
			isFlow: isFlow(actDate, config.flowHours)
		};	
	}

	job_callback(null, dataObj);
};
