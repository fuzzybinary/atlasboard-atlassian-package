/**
 * Provider strategy for Github.
 *
 * @param {object} fetch a request to fetch information from a source+repo
 * @param {String} fetch.sourceId the id of the source (used for error reporting)
 * @param {object} fetch.repository provider-specific repository object
 * @param {object} fetch.team the team information
 * @param {object} [fetch.options] provider-specific options
 * @param {object} [fetch.auth] authentication info to use
 * @param {object} dependencies
 * @param {Function} callback
 * @returns {*}
 */
module.exports = function (fetch, dependencies, callback) {
  var _ = require('lodash');
  var q = require('q');
  var getJSON = q.nbind(dependencies.easyRequest.JSON, dependencies.easyRequest);

  var githubBaseUrl = "https://api.github.com/";
  
  var validationError = validateParams();
  if(validationError) {
    return callback('error in source "' + fetch.sourceId + '": ' + validationError);
  }

  getRepoPullUrls()
    .then(getAllRepoPullRequests)
    .then(processPullRequestArray)
    .then(transformResponse)
    .nodeify(callback);


  function getAllRepoPullRequests(pullUrls) {
    return q.all(_.map(pullUrls, function(pullUrl) {
        var clearPullUrl = pullUrl.replace("{/number}", "");
        return getPrList(clearPullUrl);
    })); 
  }

  function processPullRequestArray(pullRequestArray) {
    return q.all(_.map(pullRequestArray,function(pullRequests) {
     return processRemainingPrs(pullRequests);
    }));
  }

  function transformResponse(approversArray) {
    var approvers = approversArray[0];
    
    var userResult = _.map(fetch.team, function (user) {
      return { user: user.username, PR: 0 };
    });

    _.keys(approvers).forEach(function(approver) {
      var approverObject = approvers[approver];
      var teamIndex = _.findIndex(userResult,{user: approver});
      var user = null;
      if(teamIndex >= 0) { 
        user = userResult[teamIndex];
      } else {
        user = { user: approver, PR: 0, };
        userResult.push(user);
      }
      user.PR+=approverObject.count;
      user.avatar = approverObject.avatar;
    });
    

    return _.map(userResult,function(userTuple) {
      var newUserTuple = {user: { username: userTuple.user, avatar: userTuple.avatar}, PR: userTuple.PR};
      var originalUserTuple = fetch.team[_.findIndex(fetch.team,{ username: userTuple.user })];
      if(originalUserTuple && originalUserTuple.display) {
        newUserTuple.user.display = originalUserTuple.display ;
      }
      
      if(originalUserTuple && originalUserTuple.email) {
        newUserTuple.user.email = originalUserTuple.email;
      }
      return newUserTuple;
    });

  }

  function getRepoPullUrls() {
    // If repo name is supplied, use that, otherwise get all from the project
    if (fetch.repository.repository) {
      var repoUrl = githubBaseUrl + "repos/" + fetch.repository.org + "/" + fetch.repository.repository + "/pulls"
      return q.when([repoUrl]);
    } else {
      var repoUrl = githubBaseUrl + "orgs/" + fetch.repository.org + "/repos"
      return getJSON({ url: repoUrl, headers: getAuthHeader() })
        .then(function(data) {
          if (!(data && data.length)){
            return q.reject('no data');
          }
          return q.resolve(_.map(data, function (datavalues) {
           return datavalues.pulls_url;
          }));
        });
    }

  }

  function validateParams(){
    if (!fetch.repository.org) { return 'missing org field in repository: ' + JSON.stringify(fetch.repository); }
  }

  function getAuthHeader() {
    if(fetch.auth) {
      var headers = { 'Authorization': 'token ' + fetch.auth.key,
        // This allows getting "reviewers" from the pull request API while that API is in beta
        'Accept': 'application/vnd.github.black-cat-preview+json',
        'User-Agent': 'atlasboard'
      };
      return headers; 
    } 
  }

  /**
   * Fetches list of pull requests and processes each one, counting the pending PRs.
   *
   * @param {string} nextPageUrl
   * @param {Array} [pullRequests]
   * @returns {Array} [pullRequests]
   */
  function getPrList() {
    
    var nextPageUrl = arguments[0];
    var pullRequests = arguments[1] || [];

    if (!nextPageUrl) {
      // if there are no more pages then proceed to process each PR
      return q.when(pullRequests);
    }

    return getJSON({ url: nextPageUrl, headers: getAuthHeader() })
      .then(function (data) {
        data = data && (data[0] || data);          
        if (!(data && data.length)) {
          return q.when(pullRequests);
        } else {
          //var linkHeader = response.headers['link'];
          // TODO: Need to get the "next" page out of the link header
          var next = null;
          
          // Recurse until we have built up a list of all PRs
          return getPrList(next, _.union(pullRequests, data));
        }    
      });
  }

  /**
   * Recursively fetches PR reviewers, accumulating the results in <code>users</code>, and calls <code>callback</code>
   * when done.
   *
   * @param {prs} remainingPRs an array of pull requests
   * @returns {*}
   */
  function processRemainingPrs(prs) {

    var reviewers = _.reduce(prs, function(result, pr) {
      var reviewerSet = new Set();
      _.each(pr.assignees, function(assignee) {
        reviewerSet.add(assignee);
      });
      _.each(pr.requested_reviewers, function(reviewer) {
        reviewerSet.add(reviewer)
      });

      _.each(Array.from(reviewerSet), function(reviewer) {
        var username = reviewer.login;
        var user = result[username] || (result[username] = { count: 0, avatar: reviewer.avatar_url });
        user.count += 1;
      });

      return result;
    }, {})
    
    return q.when(reviewers);
  }

};
